﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Xunit;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Unit
{
    public class IDocsDbContextFacade_SaveChanges
    {
        [Fact]
        public async Task SaveChanges_ExecutedOncePerScope()
        {
            var connectionString = "Server=10.8.0.1;" +
                "Initial Catalog=IDocsDB;" +
                "Persist Security Info=False;" +
                "User ID=iDocsDeveloper;" +
                "Password=dev_iDocsDevStream2019!;" +
                "MultipleActiveResultSets=False;" +
                "Encrypt=True;" +
                "TrustServerCertificate=True;" +
                "Connection Timeout=30;";

            var serviceCollection = new ServiceCollection();
            UseIDocsDbContextExtension.UseIDocsDbContext(serviceCollection, connectionString);

            using var firstScope = serviceCollection.BuildServiceProvider();
            var facade = firstScope.GetRequiredService<IDocsDbContextFacade>();

            var firstScopeCall = await facade.SaveChangesAsync();
            Assert.Equal(0, firstScopeCall);

            await Assert.ThrowsAsync<InvalidOperationException>(() => facade.SaveChangesAsync());

            using var secondScope = serviceCollection.BuildServiceProvider();
            facade = secondScope.GetRequiredService<IDocsDbContextFacade>();

            var secondScopeCall = await facade.SaveChangesAsync();
            Assert.Equal(0, secondScopeCall);

            await Assert.ThrowsAsync<InvalidOperationException>(() => facade.SaveChangesAsync());
        }
    }
}
