﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Xunit;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Unit
{
    public class IDocsDbContext_CanCreateDbOnCurrentState
    {
        [Fact]
        public async Task CanCreateDbOnCurrentState()
        {
            var dbContextOptions = new DbContextOptionsBuilder<IDocsDbContext>()
                .UseSqlServer(connectionString:
                    "Server=(localdb)\\MSSQLLocalDB;Integrated Security=true;Initial Catalog=idocsTestDb;")
                .Options;

            var context = new IDocsDbContext(dbContextOptions);

            await context.Database.EnsureDeletedAsync();
            await context.Database.EnsureCreatedAsync();
        }
    }
}
