using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Unit
{
    public class UseIDocsDbContextExtension_UseIDocsDbContext
    {
        [Fact]
        public async Task CanUseIDocsDbContextFromDI()
        {
            var serviceCollection = new ServiceCollection();

            var connectionString = "Server=10.8.0.1;" +
                "Initial Catalog=IDocsDB;" +
                "Persist Security Info=False;" +
                "User ID=iDocsDeveloper;" +
                "Password=dev_iDocsDevStream2019!;" +
                "MultipleActiveResultSets=False;" +
                "Encrypt=True;" +
                "TrustServerCertificate=True;" +
                "Connection Timeout=30;";

            UseIDocsDbContextExtension.UseIDocsDbContext(serviceCollection, connectionString);

            using var scope = serviceCollection.BuildServiceProvider();
            var dbContext = scope.GetRequiredService<IDocsDbContext>();

            var canConnectToDb = await dbContext.Database.CanConnectAsync();
            Assert.True(canConnectToDb);
        }
    }
}