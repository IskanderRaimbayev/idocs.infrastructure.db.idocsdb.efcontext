﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts
{
    public class IDocsDbContextFacade
    {
        private readonly IDocsDbContext _context;
        public DbSet<CompanyEntity> Companies => _context.Companies;
        public DbSet<CounterpartyEntity> Counterparties => _context.Counterparties;

        /// <summary>
        /// Used to ensure that saving all changes from change tracker to database
        /// happpens only once per single scope, typically per HTTP-request's scope.
        /// It is not thread-safe so don't use in multithreaded code.
        /// </summary>
        private bool _saveChangesCalledInCurrentScope = false;

        private const string SaveChangesAlreadyCalledError =
            $"SaveChanges operation was once already called in the current scope";

        private readonly ILogger<IDocsDbContextFacade> _logger;

        public IDocsDbContextFacade(IDocsDbContext context,
            ILogger<IDocsDbContextFacade> logger = null)
        {
            _context = context;
            _logger = logger;
        }


        /// <summary>
        /// It is not thread-safe so don't use in multithreaded code.
        /// If it was once already called in the current scope, 
        /// then <see cref="InvalidOperationException"/> will be throwed.
        /// </summary>
        public Task<int> SaveChangesAsync(CancellationToken ct = default)
        {
            if (_saveChangesCalledInCurrentScope)
            {
                _logger?.LogError(SaveChangesAlreadyCalledError);
                throw new InvalidOperationException(SaveChangesAlreadyCalledError);
            }

            _saveChangesCalledInCurrentScope = true;
            return _context.SaveChangesAsync(ct);
        }
    }
}
