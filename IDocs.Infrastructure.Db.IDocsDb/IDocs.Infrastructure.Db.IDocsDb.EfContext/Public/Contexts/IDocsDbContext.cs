﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Configurations;
using Microsoft.EntityFrameworkCore;
using System;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts
{
    [Obsolete($"Use {nameof(IDocsDbContextFacade)} instead, " +
        $"this class will be marked as an internal in future releases")]
    public class IDocsDbContext : DbContext
    {
        public DbSet<CompanyEntity> Companies { get; set; }
        public DbSet<CounterpartyEntity> Counterparties { get; set; }
        public IDocsDbContext(DbContextOptions<IDocsDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CounterpartyEntityConfiguration());
        }
    }
}
