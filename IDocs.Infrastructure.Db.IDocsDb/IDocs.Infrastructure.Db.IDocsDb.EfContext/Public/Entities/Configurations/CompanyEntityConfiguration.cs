﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Configurations
{
    internal class CompanyEntityConfiguration : 
        IEntityTypeConfiguration<CompanyEntity>
    {
        public void Configure(EntityTypeBuilder<CompanyEntity> builder)
        {
            builder.ToTable("Companies");

            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id)
                .ValueGeneratedOnAdd();

            builder.Property(p => p.NameRu)
                .HasColumnName("Name")
                .HasMaxLength(2000)
                .IsRequired();

            builder.Property(p => p.NameKz)
                .HasColumnName("NameKZ")
                .HasMaxLength(2000)
                .IsRequired();

            builder.Property(p => p.NameEn)
                .HasColumnName("NameEN")
                .HasMaxLength(2000)
                .IsRequired();

            builder.Property(p => p.Bin)
                .HasColumnName("BIN")
                .HasMaxLength(12)
                .IsRequired();

            builder.Property(p => p.Rnn)
                .HasColumnName("RNN")
                .HasMaxLength(12);

            builder.Property(p => p.Emails)
                .HasColumnName("Email")
                .HasMaxLength(1000);

            builder.Property(p => p.BillingEmail)
                .HasColumnName("BillingEmail")
                .HasMaxLength(1000);

            builder.Property(p => p.WebSite)
                .HasColumnName("WebSite")
                .HasMaxLength(200);

            builder.Property(p => p.PhoneNumber)
                .HasColumnName("PhoneNumber")
                .HasMaxLength(200);

            builder.Property(p => p.PhoneCode)
                .HasColumnName("PhoneCode")
                .HasMaxLength(10);

            builder.Property(p => p.MobilePhoneNumber)
                .HasColumnName("MobilePhoneNumber")
                .HasMaxLength(200);

            builder.Property(p => p.AddressRu)
                .HasColumnName("AddressRu")
                .HasMaxLength(2000);

            builder.Property(p => p.AddressKz)
                .HasColumnName("AddressKz")
                .HasMaxLength(2000);

            builder.Property(p => p.Kogd)
                .HasColumnName("Kogd")
                .HasMaxLength(10);

            builder.Property(p => p.ImageBase64)
                .HasColumnName("ImageBase64");


        }
    }
}
