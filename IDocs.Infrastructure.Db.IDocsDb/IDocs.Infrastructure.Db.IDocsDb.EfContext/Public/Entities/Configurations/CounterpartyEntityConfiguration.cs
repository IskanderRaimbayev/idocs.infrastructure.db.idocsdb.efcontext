﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Configurations
{
    internal class CounterpartyEntityConfiguration :
        IEntityTypeConfiguration<CounterpartyEntity>
    {
        public void Configure(EntityTypeBuilder<CounterpartyEntity> builder)
        {
            builder.ToTable("CounterpartiesV2");

            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id)
                .ValueGeneratedOnAdd();

            builder.HasOne<CompanyEntity>()
                .WithMany()
                .HasForeignKey(p => p.InitiatorCompanyId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne<CompanyEntity>()
                .WithMany()
                .HasForeignKey(p => p.CounterpartyCompanyId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
