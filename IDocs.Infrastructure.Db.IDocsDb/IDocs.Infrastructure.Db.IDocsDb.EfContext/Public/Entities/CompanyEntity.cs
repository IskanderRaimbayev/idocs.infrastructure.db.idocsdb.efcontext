﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Base;
using System;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities
{
    public class CompanyEntity : ITrackableEntity, ISoftDeleteableEntity
    {
        public Guid Id { get; set; }
        public string NameRu { get; set; }
        public string NameKz { get;set; }
        public string NameEn { get; set; }
        public string Bin { get; set; }
        public string Rnn { get; set; }
        public string Emails { get; set; }
        public string BillingEmail { get; set; }
        public string WebSite { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneCode { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string AddressRu { get; set; }
        public string AddressKz { get; set; }
        public string Kogd { get; set; }
        public string ImageBase64 { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public string LastUpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? DeleteId { get; set; }
    }

}
