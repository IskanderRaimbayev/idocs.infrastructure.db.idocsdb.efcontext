﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Base;
using System;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities
{
    public class CounterpartyEntity : ITrackableEntity
    {
        public Guid Id { get; set; }
        public Guid InitiatorCompanyId { get; set; }
        public Guid CounterpartyCompanyId { get; set; }
        public string ContactEmails { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
