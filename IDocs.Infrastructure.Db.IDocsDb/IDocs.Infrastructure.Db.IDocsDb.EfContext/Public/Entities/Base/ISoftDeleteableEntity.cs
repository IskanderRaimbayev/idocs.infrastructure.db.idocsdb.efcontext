﻿using System;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Base
{
    public interface ISoftDeleteableEntity
    {
        bool IsDeleted { get; set; }
        Guid? DeleteId { get; set; }
    }
}
