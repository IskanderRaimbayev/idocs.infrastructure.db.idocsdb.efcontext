﻿using System;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities.Base
{
    public interface ITrackableEntity
    {
        DateTime CreatedOn { get; set; }
        string CreatedBy { get; set; }
        DateTime? LastUpdatedOn { get; set; }
        string LastUpdatedBy { get; set; }
    }
}
