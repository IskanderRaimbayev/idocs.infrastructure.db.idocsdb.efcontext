﻿using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Extensions
{
    public static class UseIDocsDbContextExtension
    {
        private static void UseSqlServerOption(
            IServiceCollection services, string connectionString, int retryOnFailureCount = 3)
        {
            services.AddDbContext<IDocsDbContext>(options =>
            {
                options.UseSqlServer(connectionString, sqlServerOptions =>
                {
                    sqlServerOptions.EnableRetryOnFailure(retryOnFailureCount);
                });
            });
        }

        private static void UseInMemoryOption(
            IServiceCollection services, string databaseName)
        {
            services.AddDbContext<IDocsDbContext>(options =>
            {
                options.UseInMemoryDatabase(databaseName: databaseName);
            });
        }

        public static IServiceCollection UseIDocsDbContext(
            this IServiceCollection services, string connectionString, 
            int retryOnFailureCount = 3)
        {
            UseInMemoryOption(services, "IDocsDbContext");
            services.AddScoped<IDocsDbContextFacade>();
            return services;
        }
    }
}
